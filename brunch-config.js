module.exports = {
	files : {
		javascripts: { 
			joinTo: {
				"assets/js/app.js": /^(dev\/js\/.*\.coffee)/, "assets/js/vendor.js": /^(dev\/js\/lib)|(deps)/
			}
		},
		stylesheets: {joinTo: 'assets/css/style.css'}
	},
	paths : {
		public: '',
		watched: ['dev']
	},
	plugins : {
		browserSync: {
			files: ["*.php", '*.twig']
		}
	},
	// Disable CommonJS modules
	npm: {
		enabled: false
	},
	modules: {
		wrapper: false,
		definition: false
	}
}