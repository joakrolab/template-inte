$ = jQuery.noConflict()

$ ->
	init()

init = ->

	# Global JS
	App.init()
	template = $('#content').attr('data-template')
	page = $('#content').attr('data-page')

	switch template
		when 'homepage'


destroy = ->
	App.destroy()
