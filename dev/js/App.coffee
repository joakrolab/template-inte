App =  {

	controller : false
	wHeight : false
	isMobileMenuOpen : false
	isMobileCartOpen : false
	tlShowCart : false
	
	init : ->
		console.log 'App init'

		_this = @
		@controller = new ScrollMagic.Controller()
		@wHeight = $(window).height()
		$(window).on "resize", (ev) ->
			_this.wHeight = $(window).height()

		@isMobileMenuOpen = false
		@isMobileCartOpen = false


		@revealManager()

		@debugManager()

	revealManager : ->

		_this = @

		$('[gsap-reveal]').each ->
			tween = TweenLite.fromTo @, 1, {y:40, autoAlpha:0}, {y:0, autoAlpha:1, ease:Quad.easeOut}
			scene = new ScrollMagic.Scene({triggerElement: @, triggerHook: 0.9, duration: _this.wHeight / 3})
				.setTween tween
				.addTo _this.controller

	debugManager : ->
		debugGrid = $('.susy-grid').css('background-image')
		
		$(window).keypress (ev) ->
			if ev.keyCode == 100
				if $('.main-wrapper').css('background-image') == 'none'
					$('.main-wrapper').css('background-image', debugGrid)
				else
					$('.main-wrapper').css('background-image', 'none')

	destroy : ->
		console.log "App destroy"
		
}